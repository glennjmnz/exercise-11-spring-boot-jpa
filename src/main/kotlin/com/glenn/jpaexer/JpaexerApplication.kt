package com.glenn.jpaexer

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class JpaexerApplication

fun main(args: Array<String>) {
	runApplication<JpaexerApplication>(*args)
}
